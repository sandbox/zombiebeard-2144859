<?php

/**
 * Checkout pane callback: returns a pane allowing the customer to select a
 * layaway option for their order.
 */
function commerce_layaway_checkout_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_layaway') . '/includes/commerce_layaway.checkout_pane.inc';

  $pane_form = array();
  $pane_values = !empty($form_state['values'][$checkout_pane['pane_id']]) ? $form_state['values'][$checkout_pane['pane_id']] : array();
  if (isset($pane_values['layaway'])) {
    $default = $pane_values['layaway'];
  }
  else {
    $default = db_select('commerce_layaway_order', 'lo')
      ->fields('lo', array('layaway_id'))
      ->condition('order_id', $order->order_id, '=')
      ->execute()
      ->fetchField();
  }

  $options = commerce_layaway_options();

  // If at least one layaway option is available.
  if (!empty($options)) {
    $options = array('_none' => t('None')) + $options;

    $pane_form['layaway'] = array(
      '#type' => 'select',
      '#title' => t('Options'),
      '#options' => $options,
      '#default_value' => $default,
    );
  }
  else {
    // Otherwise display a message indicating there are no layaway options
    // available.
    $message = t('No valid layaway options found for your order.');

    $pane_form['message'] = array(
      '#markup' => '<div>' . $message . '</div>',
      '#weight' => -20,
    );
  }

  return $pane_form;
}

/**
 * Checkout pane callback: submit the layaway checkout pane.
 */
function commerce_layaway_checkout_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  //drupal_set_message('<pre>Line:' . __LINE__ . " \$form_state = " . check_plain(print_r($form_state, TRUE)) . '</pre>');
  $pane_id = $checkout_pane['pane_id'];
  $pane_form = $form[$pane_id];
  $pane_values = $form_state['values'][$pane_id];

  // Delete any existing layaways for this order.
  db_delete('commerce_layaway_order')
    ->condition('order_id', $order->order_id, '=')
    ->execute();

  if ($pane_values['layaway'] != '_none') {
    if (empty($result)) {
      db_insert('commerce_layaway_order')
        ->fields(array(
          'layaway_id' => $pane_values['layaway'],
          'order_id' => $order->order_id,
        ))
        ->execute();
    }
  }
}