<?php

/**
 * @file
 * Layaway editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class CommerceLayawayUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = t('Layaways');
    $items[$this->path]['description'] = 'Manage layaway entities, including adding, editing and deleting.';

    // Change the title of the add page.
    $items[$this->path . '/add']['title'] = t('Add Layaway');
    unset($items[$this->path . '/add']['title callback']);
    unset($items[$this->path . '/add']['title arguments']);

    return $items;
  }
}

function commerce_layaway_form($form, &$form_state, $layaway, $op) {
  $form = array();

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($layaway->label) ? $layaway->label : '',
  );
  $form['modifier'] = array(
    '#title' => t('Modifier'),
    '#type' => 'select',
    '#options' => array(
      '_none' => t('None'),
      '/' => t('/ (Division)'),
      '*' => t('% (Percentage)'),
    ),
    '#description' => t('To add a flat fee to the layaway amount leave Modifier at None.'),
    '#default_value' => isset($layaway->modifier) ? $layaway->modifier : array(),
  );
  $form['modifier_amount'] = array(
    '#title' => t('Modifier Amount'),
    '#type' => 'textfield',
    '#description' => t('Your fee will be ($order_total (/ or *) $modifier_amount). !note if you want to modify by 10 percent you need to enter .10 not 10.', array('!note' => '<strong>NOTE::</strong>')),
    '#default_value' => isset($layaway->modifier_amount) ? $layaway->modifier_amount : '',
    '#states' => array(
      'invisible' => array(
        ':input[name="modifier"]' => array('value' => '_none'),
      ),
    ),
  );
  $form['fee'] = array(
    '#title' => t('Fee'),
    '#type' => 'textfield',
    '#description' => t('Your fee will be a flat rate unless you select a modifier.'),
    '#default_value' => isset($layaway->fee) ? $layaway->fee : '',
    '#states' => array(
      'visible' => array(
        ':input[name="modifier"]' => array('value' => '_none'),
      ),
    ),
  );
  $form['number_payments'] = array(
    '#title' => t('Number of Payments'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Number of payments including the initial payment. So if you want the user to pay 4 times total there would be the initial checkout and 3 additional charges.'),
    '#default_value' => isset($layaway->number_payments) ? $layaway->number_payments : '',
  );
  $form['payment_interval'] = array(
    '#title' => t('Payment Interval'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('How often to charge.'),
    '#default_value' => isset($layaway->payment_interval) ? $layaway->payment_interval : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function commerce_layaway_form_submit($form, &$form_state) {
  $layaway = entity_ui_form_submit_build_entity($form, $form_state);
  $layaway->save();
  // @todo - Look into a better way to handle the redirect.
  $form_state['redirect'] = 'admin/commerce/layaway';
}